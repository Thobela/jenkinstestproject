﻿using System.Configuration;

namespace EAAutoFramework.ConfigElement
{
    public class EAFrameworkElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name { get { return (string)base["name"]; } }

        [ConfigurationProperty("aut", IsRequired =true)]
        public string AUT { get { return (string)base["aut"]; } }

        [ConfigurationProperty("browser", IsRequired = true)]
        public string Browser { get { return (string)base["browser"]; } }

        [ConfigurationProperty("testType", IsRequired = true)]
        public string TestType { get { return (string)base["testType"]; } }

        [ConfigurationProperty("isLog", IsRequired = true)]
        public string IsLog { get { return (string)base["isLog"]; } }

        [ConfigurationProperty("logPath", IsRequired = true)]
        public string LogPath { get { return (string)base["logPath"]; } }

        //[ConfigurationProperty("buildName", IsRequired = true)]
        //public string BuildName { get { return (string)base["buildName"]; } }

       // [ConfigurationProperty("isReporting", IsRequired = true)]
        //public string IsReporting { get { return (string)base["isReporting"]; } }

        [ConfigurationProperty("autConnectionString", IsRequired =true)]
        public string AUTConnectionString { get { return (string)base["autConnectionString"]; } }

        //[ConfigurationProperty("Username", IsRequired =true)]
        //public string UserName { get { return (string)base["UserName"]; } }
    }
}
