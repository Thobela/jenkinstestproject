﻿using System;
using EAAutoFramework.Base;
using EAAutoFramework.ConfigElement;


namespace EAAutoFramework.Config
{
    public class ConfigReader
    {
        public static void SetFrameworkSettings()
        {
            Settings.AUT = EATestConfiguration.EASettings.TestSettings["production"].AUT;
            Settings.TestType = EATestConfiguration.EASettings.TestSettings["production"].TestType;
            Settings.IsLog = EATestConfiguration.EASettings.TestSettings["production"].IsLog;
            ///Settings.IsReporting = EATestConfiguration.EASettings.TestSettings["staging"].IsReporting;
            //Settings.BuildName = EATestConfiguration.EASettings.TestSettings["staging"].BuildName;
            Settings.LogPath = EATestConfiguration.EASettings.TestSettings["production"].LogPath;
            Settings.AppConnectionString = EATestConfiguration.EASettings.TestSettings["production"].AUTConnectionString;
            Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), EATestConfiguration.EASettings.TestSettings["production"].Browser);
        }

    }
}
