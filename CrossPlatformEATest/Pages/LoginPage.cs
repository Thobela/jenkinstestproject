﻿using OpenQA.Selenium;
using EAAutoFramework.Base;
using EAAutoFramework.Extensions;
using OpenQA.Selenium.Remote;

namespace CrossPlatformEATest.Pages
{
    internal class LoginPage : BasePage
    {
        /// <summary>
        /// Objects for Login Page
        /// </summary>
        /// 
        IWebElement txtUsername => DriverContext.Driver.FindElement(By.Id("UserName"));

        IWebElement txtPassword => DriverContext.Driver.FindElement(By.Id("Password"));

        IWebElement btnLogin => DriverContext.Driver.FindElement(By.CssSelector("input.btn"));

        public void Login(string userName, string password)
        {
            txtUsername.SendKeys(userName);
            txtPassword.SendKeys(password);
        }

        public HomePage ClickLoginButton()
        {
            btnLogin.Submit();
            return GetInstance<HomePage>();
        }

        internal void CheckIFLoginExist()
        {
            txtUsername.AssertElementPresent();
        }
    }
}
