﻿using EAAutoFramework.Base;
using EAAutoFramework.Extensions;
using OpenQA.Selenium;

namespace CrossPlatformEATest.Pages
{
    internal class HomePage : BasePage
    {
        IWebElement lnkLogin => DriverContext.Driver.FindElement(By.LinkText("Login"));

        IWebElement lnkEmployeeList => DriverContext.Driver.FindElement(By.LinkText("Employee List"));

        IWebElement lnkLoggedInUser => DriverContext.Driver.FindElement(By.XPath("//a[@title='Manage']"));

        IWebElement lnkLlnkLogoffogin => DriverContext.Driver.FindElement(By.LinkText("Log off"));

        internal LoginPage ClickLogin()
        {
            lnkLogin.Click();
            return GetInstance<LoginPage>();
        }

        internal string GetLoggedInUser()
        {
            return lnkLoggedInUser.GetLinkText();
        }

        public EmployeeListPage ClickEmployeeList()
        {
            lnkEmployeeList.Click();

            return GetInstance<EmployeeListPage>();
        }

        internal void CheckIfLoginExist()
        {
            lnkLogin.AssertElementPresent();
        }
    }
}
